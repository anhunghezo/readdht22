# DHT22 sensor reader

A small QML based application which utilizes C++ class for high-speed GPIO 
polling to handle DHT22 sensor output.

A simple projects consist of two parts: QML code and C++ stuff 
which is represented by three classes. 

The QML stuff is responsible for setting up the GPIO pin number for polling:


![alt text](images/dht22_1.jpeg)



and displaying the current temperature and humidity: 


![alt text](images/dht22_2.jpeg)


The first C++ class "DhtManager" is exposing the data to QML code and connecting 
QML code with C++ class methods. A "DhtWorker" class provides reading Dht sensor 
in the separated thread.  And the last class is "RpiGpioBasic" which controls 
the directions and states of GPIO pins using a registry access (mmaping into /dev/mem).

It has been tested on the Raspberry Pi 3 running Boot2Qt image version 5.12.3.

![alt text](images/RPI3_Termometer.jpg)
