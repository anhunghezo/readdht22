QT += quick quickcontrols2 widgets
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(RPI3,RPI3|COLIBRI_IMX6|Desktop): DEFINES+=RPI_PLATFORM
CONFIG(COLIBRI_IMX6,RPI3|COLIBRI_IMX6|Desktop): DEFINES+=COLIBRI_PLATFORM
CONFIG(Desktop,RPI3|COLIBRI_IMX6|Desktop): DEFINES+=DESKTOP_PLATFORM

HEADERS += \
    DhtWorker.h \
    RpiGpioBasic.h \
    DhtManager.h

SOURCES += \
    DhtWorker.cpp \
    main.cpp \
    RpiGpioBasic.cpp \
    DhtManager.cpp

RESOURCES += qml.qrc

RPI3 {
    message("Target Platform: Raspberry Pi")

    # Additional import path used to resolve QML modules in Qt Creator's code model
    QML_IMPORT_PATH=/opt/b2qt/2.5.3/sysroots/cortexa7hf-neon-vfpv4-poky-linux-gnueabi/usr/lib/qml
    # Default rules for deployment.
    target.path = /home/root
    INSTALLS += target
}

COLIBRI_IMX6 {
    message("Target Platform: Colibri-imx6")

    # Default rules for deployment.
    target.path = /home/root
    INSTALLS += target
}

Desktop {
    message("Target Platform: Desktop")
    # Additional import path used to resolve QML modules in Qt Creator's code model
    QML_IMPORT_PATH=/opt/Qt5.12.4/5.12.4/gcc_64/plugins
}
# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =
