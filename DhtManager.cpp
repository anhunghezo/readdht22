/**
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020 Val Vachaev <http://www.vachaev.net>

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "DhtManager.h"
#include "DhtWorker.h"
#include <QThread>
#include <QDebug>
#include <vector>

const uint m_pollInterval = 5000; // milliseconds

DhtManager::DhtManager(QObject *parent) : QObject(parent),
    m_gpioNum(0),
    szTemperature("00.0"),
    szHumidity("00.0"),
    m_isPolling(false),
    m_pWorker(nullptr)
{
}

DhtManager::~DhtManager()
{
    if (m_pWorker)
    {
        m_pWorker->stop();
        delete m_pWorker;
    }
}

void DhtManager::initDHTMonitorPin(QString szNum)
{
    // a list of available GPIO pins
    std::vector<int> gpioList = {2, 3, 4, 5, 6, 7, 8, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};
    m_gpioNum = szNum.toInt();

    std::vector<int>::iterator it = std::find(gpioList.begin(), gpioList.end(), m_gpioNum);

    if (it == gpioList.end())
    {
        qCritical("GPIO # %d is not available", m_gpioNum);
        m_gpioNum = 0;
    }
}

void DhtManager::startPolling()
{
    if (m_isPolling || m_gpioNum == 0)
    {
        qCritical("Failed to start a thread...");
        return;
    }

    m_pWorker = new DhtWorker(m_gpioNum, m_pollInterval);
    QThread* pThread = new QThread;

    connect(m_pWorker, SIGNAL(finished()), pThread, SLOT(quit()));
    connect(m_pWorker, SIGNAL(finished()), m_pWorker, SLOT(deleteLater()));
    connect(m_pWorker, &DhtWorker::sigResult, this, &DhtManager::slotDone, Qt::QueuedConnection);
    connect(pThread, SIGNAL(finished()),  pThread, SLOT(deleteLater()));
    // launch a worker in the separate thread
    connect(pThread, &QThread::started, m_pWorker, &DhtWorker::run);
    connect(this, SIGNAL(sigFinish()), m_pWorker, SLOT(slotFinish()));

    // magic move
    m_pWorker->moveToThread(pThread);
    pThread->start();
}

void DhtManager::slotDone(double t, double h)
{
    QString sz = QString::number(t, 'f', 2);

    if (sz != szTemperature)
    {
        szTemperature = sz;
        emit szTemperatureChanged();
    }

    sz = QString::number(h, 'f', 2);
    if (szHumidity != sz)
    {
        szHumidity = sz;
        emit szHumidityChanged();
    }
}
