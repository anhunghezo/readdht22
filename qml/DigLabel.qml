import QtQuick 2.12
import QtQuick.Controls 2.12
import lcdnumberplugin 1.0

Item {
    id: rootNum
    width: 200
    height: 100

    property alias text: lcdId.text
    property alias textLabel: numLabel.text

    Rectangle {
        id: ledNum
        border.width: 2
        border.color: "steelblue"
        radius: 5

        width: parent.width
        height: parent.height - 15

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 15

        color: "transparent"
    }

    Rectangle {
        anchors.top: parent.top
        width: parent.width - 40
        height: 30
        anchors.horizontalCenter: parent.horizontalCenter
        color: "black"

        Text {
            id: numLabel
            text: DHTCtrl.szHumidity
            color: "steelblue"
            font.pixelSize: 24
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    LCDNumber {
        id: lcdId
        anchors.fill: parent
        anchors.margins: 10
        anchors.topMargin: 30

        digitCount: 4
        textColor: "#4b96f3"
        anchors.verticalCenter: parent.verticalCenter

        Timer {
            interval: 500; running: true; repeat: true
            onTriggered: {
                lcdId.update()
            }
        }
    }
}
